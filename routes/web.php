<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AnnuaryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Annuary routess

Route::get('/',[AnnuaryController::class, 'index'])->name('annuaries');
Route::get('annuaries/my-favourites', [AnnuaryController::class, 'my_favourites'])->name('annuaries.my_favourites');
Route::get('annuary/create', [AnnuaryController::class, 'create'])->name('annuary.create');
Route::post('annuary/store', [AnnuaryController::class, 'store'])->name('annuary.store');
Route::get('annuary/{annuary}/show', [AnnuaryController::class, 'show'])->name('annuary.show');
Route::get('annuary/{annuary}/edit', [AnnuaryController::class, 'edit'])->name('annuary.edit');
Route::put('annuary/{annuary}/update', [AnnuaryController::class, 'update'])->name('annuary.update');
Route::delete('annuary/{annuary}/destroy', [AnnuaryController::class, 'destroy'])->name('annuary.destroy');



