<?php

namespace App\Http\Controllers;

use App\Models\Annuary;
use Illuminate\Http\Request;

class AnnuaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $annuaries = Annuary::orderBy('updated_at', 'desc')->get();

        return view('annuaries', compact('annuaries', 'request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('annuaries', compact('request'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'firstname' =>  'required',
            'lastname' =>  'required',
            'email' =>   'required',
            'phone' =>   'required',
            'address' =>   'required',
        ]);

        Annuary::create([
            'firstname' =>  $request->firstname,
            'lastname' =>  $request->lastname,
            'email' =>   $request->email,
            'phone' =>   $request->phone,
            'address' =>   $request->address,
            "photo" => 'photo.png'
        ]);

        return redirect()->route('annuaries')->with('success', 'Added succesfuly !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Annuary  $annuary
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Annuary $annuary)
    {
        return view('annuaries', compact('request', 'annuary'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Annuary  $annuary
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Annuary $annuary)
    {
        return view('annuaries', compact('request', 'annuary'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Annuary  $annuary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Annuary $annuary)
    {
        $request->validate([
            'firstname' =>  'required',
            'lastname' =>  'required',
            'email' =>   'required',
            'phone' =>   'required',
            'address' =>   'required',
        ]);

        $annuary->update([
            'firstname' =>  $request->firstname,
            'lastname' =>  $request->lastname,
            'email' =>   $request->email,
            'phone' =>   $request->phone,
            'address' =>   $request->address
        ]);

        return redirect()->route('annuaries')->with('success', 'Updated succesfuly !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Annuary  $annuary
     * @return \Illuminate\Http\Response
     */
    public function destroy(Annuary $annuary)
    {
        $annuary->delete();

        return redirect()->route('annuaries')->with('success', 'Deleted succesfuly !');
    }

    /**
     * Display a listing of my favourites resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function my_favourites(Request $request)
    {
        $my_favourites = Annuary::orderBy('updated_at', 'desc')->get();

        return view('annuaries', compact('my_favourites', 'request'));
    }
}
