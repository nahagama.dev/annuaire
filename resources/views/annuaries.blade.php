<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

        <title>{{ __('Annuary-app') }}</title>
    </head>

    <body>
        <div class="container">
            <h3 class="mt-5 fw-bold">{{ __('Annuary app') }}</h3>

            <nav class="mt-5 pb-5">
                <div class="nav nav-tabs h5" id="nav-tab" role="tablist">
                    <a class="nav-link @if($request->route()->named('annuaries')) active @endif" href={{ route('annuaries') }}>{{ __('Home') }}</a>
                    <a class="nav-link @if($request->route()->named('annuary.create') || $request->route()->named('annuary.edit') || $request->route()->named('annuary.show')) active @endif" href={{ route('annuary.create') }}>
                        @if($request->route()->named('annuary.edit')){{ __('Edit') }}
                        @elseif($request->route()->named('annuary.show')) {{ __('Show') }}
                        @else() {{ __('New') }} {{ __(' contact') }}
                        @endif
                    </a>
                    <a class="nav-link @if($request->route()->named('annuaries.my_favourites')) active @endif" href={{ route('annuaries.my_favourites') }}>{{__('My favourites') }}</a>
                </div>
            </nav>

            @if($request->route()->named('annuaries'))
                <h5 class="text-muted" for="formFile" class="form-label">{{ __('All contacts list') }}</h5>

                @if (session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ __('Congratulations, ') }}</strong> {{ session('success') }}

                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                <div class="my-3 p-3 bg-body rounded shadow-sm">
                    @if ($annuaries->count() == 0)
                        <h5 class="fw-bold">{{ __('No data !') }}</h5>
                    @else
                        @foreach ($annuaries as $annuary)
                        <div class="d-flex text-muted pt-3">
                            <img class="bd-placeholder-img flex-shrink-0 me-2 rounded" width="32" height="32" src="{{$annuary->photo}}" alt="">

                            <div class="pb-3 mb-0 small border-bottom w-100">
                                <div class="d-flex justify-content-between">
                                    <strong class="text-gray-dark">{{ $annuary->firstname }} {{ $annuary->lastname }}</strong>

                                    <div class="btn-group">
                                        <button class="btn btn-outline-secondary" type="button" id="defaultDropdown" data-bs-toggle="dropdown" data-bs-auto-close="true" aria-expanded="false">
                                            {{ __('...') }}
                                        </button>

                                        <ul class="dropdown-menu" aria-labelledby="defaultDropdown">
                                            <li><a class="dropdown-item text-primary" href="{{ route('annuary.show', $annuary) }}">{{ __('Show') }}</a></li>
                                            <li><a class="dropdown-item text-primary" href="{{ route('annuary.edit', $annuary) }}">{{ __('Edit') }}</a></li>
                                            <li>
                                                <form action="{{ route('annuary.destroy', $annuary) }}" method="post">
                                                    @csrf

                                                    @method('delete')

                                                    <button class="dropdown-item text-danger" type="submit">{{ __('Delete') }}</button>
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @endif
                </div>
            @endif

            @if($request->route()->named('annuary.create')  || $request->route()->named('annuary.edit'))
                <div class="b-example-divider"></div>

                <div class="modal modal-signin position-static d-block bg-secondary py-5" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content rounded-5 shadow">

                        <div class="modal-header p-5 pb-4 border-bottom-0">
                            <h2 class="fw-bold mb-0">
                                @if ($request->route()->named('annuary.create'))
                                    {{ __('Add a new contact')}}
                                @endif

                                @if ($request->route()->named('annuary.edit'))
                                    {{ __('Edit contact')}}
                                @endif
                            </h2>
                        </div>

                        <div class="modal-body p-5 pt-0">
                            <form class="" action="@if(isset($annuary)) {{ route('annuary.update', $annuary) }} @else {{ route('annuary.store') }} @endif" method="POST" enctype="multipart/form-data">
                                @csrf

                                @isset($annuary)
                                    @method('put')
                                @endisset
                                <div class="form-floating mb-3">
                                    <input class="form-control rounded-4" id="firstname" name="firstname" placeholder="" value="@if(isset($annuary)) {{ $annuary->firstname }} @else {{ old('firstname') }} @endif">
                                    <label for="firstname">{{ __('Firstname')}}</label>
                                </div>

                                <div class="form-floating mb-3">
                                    <input class="form-control rounded-4" id="lastname" name="lastname" placeholder="" value="@if(isset($annuary)) {{ $annuary->lastname }} @else {{ old('lastname') }} @endif">
                                    <label for="lastname">{{ __('Lastname')}}</label>
                                </div>

                                <div class="form-floating mb-3">
                                    <input type="email" class="form-control rounded-4" id="email" name="email" placeholder="" value="@if(isset($annuary)) {{ $annuary->email }} @else {{ old('email') }} @endif">
                                    <label for="email">{{ __('Email')}}</label>
                                </div>

                                <div class="form-floating mb-3">
                                    <input class="form-control rounded-4" id="phone" name="phone" placeholder="" value="@if(isset($annuary)) {{ $annuary->phone }} @else {{ old('phone') }} @endif">
                                    <label for="phone">{{ __('Phone')}}</label>
                                </div>

                                <div class="form-floating mb-3">
                                    <input class="form-control rounded-4" id="address" name="address" placeholder="" value="@if(isset($annuary)) {{ $annuary->address }} @else {{ old('address') }} @endif">
                                    <label for="address">{{ __('Address')}}</label>
                                </div>

                                <div class="mb-3">
                                    <input class="form-control" type="file" id="formFile">
                                    <small class="text-muted" for="formFile" class="form-label">Photo</small>
                                </div>

                                <button class="w-100 mb-2 btn btn-lg rounded-4 btn-primary" type="submit">@if(isset($annuary))
                                    {{ __('Update') }}
                                    @else
                                        {{ __('Save') }}
                                    @endif

                                {{ __('  contact') }}</button>

                                <hr class="my-4">
                            </form>
                        </div>
                        </div>
                    </div>
                </div>

                <div class="b-example-divider"></div>
            @endif

            @if($request->route()->named('annuary.show'))
                <div class="b-example-divider"></div>

                <div class="modal modal-signin position-static d-block bg-secondary py-5" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content rounded-5 shadow">

                            <div class="modal-header p-5 pb-4 border-bottom-0">
                                <h2 class="fw-bold mb-0">
                                    {{ __('Contact details') }}
                                </h2>
                            </div>

                            <div class="modal-body p-5 pt-0">
                                <div>
                                    <img class="bd-placeholder-img flex-shrink-0 me-2 rounded" width="64" height="64" src="{{$annuary->photo}}" alt="">
                                </div>

                                <hr>

                                <div>
                                    <p>{{ __('Firstname : ') }}</p>

                                    <span class="fw-bold">{{ $annuary->firstname }}</span>
                                </div>

                                <hr>

                                <div>
                                    <p>{{ __('Lastname : ') }}</p>

                                    <span class="fw-bold">{{ $annuary->lastname }}</span>
                                </div>

                                <hr>


                                <div>
                                    <p>{{ __('Email : ') }}</p>

                                    <span class="fw-bold">{{ $annuary->email }}</span>
                                </div>

                                <hr>


                                <div>
                                    <p>{{ __('Phone : ') }}</p>

                                    <span class="fw-bold">{{ $annuary->phone }}</span>
                                </div>

                                <hr>


                                <div>
                                    <p>{{ __('Address : ') }}</p>

                                    <span class="fw-bold">{{ $annuary->address }}</span>
                                </div>

                                <hr class="my-4">

                                <div class="d-flex justify-content-end">
                                    <a class="btn btn-primary btn-sm" style="margin-right:5px" href="{{ route('annuary.edit', $annuary) }}"> {{ __('Edit') }}</a>

                                    <form action="{{ route('annuary.destroy', $annuary) }}" method="post">
                                        @csrf

                                        @method('delete')

                                        <button class="btn btn-danger btn-sm" type="submit">{{ __('Delete') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            @endif
        </div>




        <!-- Optional JavaScript; choose one of the two! -->

        <!-- Option 1: Bootstrap Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

        <!-- Option 2: Separate Popper and Bootstrap JS -->
        <!--
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
        -->
    </body>
</html>
